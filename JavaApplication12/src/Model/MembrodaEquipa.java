/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Bruno
 */
public class MembrodaEquipa {
    private int numMec;
    private String nome;
    private String email;
    private int idade;

    public MembrodaEquipa()
    {
    
    }
    
    public MembrodaEquipa(int numMec, String nome,String email,int idade)
    {
        this.numMec=numMec;
        this.nome=nome;
        this.email=email;
        this.idade=idade;
        
    }

    /**
     * @return the numMec
     */
    public int getNumMec() {
        return numMec;
    }

    /**
     * @param numMec the numMec to set
     */
    public void setNumMec(int numMec) {
        this.numMec = numMec;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @param idade the idade to set
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    
}
