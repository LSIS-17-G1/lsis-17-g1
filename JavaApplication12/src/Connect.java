
import java.sql.*;

/**
 *
 * @author Henrique
 */
public class Connect {

    public Connection conn = null;

    public Connect() {
    }

    public Connection Connect() {   // ligação BD tem de dar return da conecção para distribuir para os métodos
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/lsis", "root", "");
        } catch (SQLException ex) {
            System.out.print("SQLException: " + ex.getMessage());
        }
        return conn;
    }
}
