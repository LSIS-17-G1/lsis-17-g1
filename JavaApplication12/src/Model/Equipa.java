/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Bruno
 */
public class Equipa {
    
    private int id;
    private String nome;
    private MembrodaEquipa membroEquipa;
    private int numElementos;
    private String faculdade;

    public Equipa()
    {
    
    }
    
    public Equipa(int id, String nome,MembrodaEquipa membroEquipa,int numElementos, String faculdade)
    {
        this.id=id;
        this.nome=nome;
        this.membroEquipa=membroEquipa;
        this.numElementos=numElementos;
        this.faculdade=faculdade;
        
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the numElementos
     */
    public int getNumElementos() {
        return numElementos;
    }

    /**
     * @param numElementos the numElementos to set
     */
    public void setNumElementos(int numElementos) {
        this.numElementos = numElementos;
    }

    /**
     * @return the membroEquipa
     */
    public MembrodaEquipa getMembroEquipa() {
        return membroEquipa;
    }

    /**
     * @param membroEquipa the membroEquipa to set
     */
    public void setMembroEquipa(MembrodaEquipa membroEquipa) {
        this.membroEquipa = membroEquipa;
    }

    /**
     * @return the faculdade
     */
    public String getFaculdade() {
        return faculdade;
    }

    /**
     * @param faculdade the faculdade to set
     */
    public void setFaculdade(String faculdade) {
        this.faculdade = faculdade;
    }

}
