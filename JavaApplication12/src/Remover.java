/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.*;
import java.util.Scanner;

/**
 *
 * @author Henrique
 */
public class Remover {

    Scanner scan = new Scanner(System.in);
    public Connection con;
    public PreparedStatement preparedStatement;

    public Remover(Connection con) {
        this.con = con;
    }

    public void Elementos() {
        try {
            String sql = "Delete from elementos where elementos.numero = ?";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Insira o numero do elemento que deseja eliminar:");
            int numero = scan.nextInt();
            preparedStatement.setInt(1, numero);
            preparedStatement.executeUpdate();
            System.out.print("Eliminado Com Sucesso!");
        } catch (SQLException ex) {
            System.out.print("Error Message:" + ex.getMessage());
        }
    }

    public void Equipas() {
        try {
            String sql = "Delete from equipa where equipa.idEquipa = ?";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Insira o numero da equipa que deseja eliminar:");
            String id = scan.next();
            preparedStatement.setString(1, id);
            preparedStatement.executeUpdate();
            System.out.print("Eliminado Com Sucesso!");
        } catch (SQLException ex) {
            System.out.print("Error Message:" + ex.getMessage());
        }

    }

    public void deleteProva() {
        try {
            String sql = "Delete from prova where equipa.idEquipa = ?";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Insira o ID da prova que deseja eliminar:");
            int id = scan.nextInt();
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.print("Error Message:" + ex.getMessage());
        }

    }

    public void Robo() {
        try {
            String sql = "Delete from robo where robo.id = ?";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Insira o ID do robo que deseja eliminar:");
            int ID = scan.nextInt();
            preparedStatement.setInt(1, ID);
            preparedStatement.executeUpdate();
            System.out.print("Eliminado Com Sucesso!");
        } catch (SQLException ex) {
            System.out.print("Error Message:" + ex.getMessage());
        }

    }

}
