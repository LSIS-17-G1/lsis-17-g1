/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.*;
import java.sql.PreparedStatement;
import java.util.Scanner;

/**
 *
 * @author Henrique
 */
public class Adicionar {

    Scanner scan = new Scanner(System.in);
    public Connection con;          // Ligações necessarias
    public PreparedStatement preparedStatement;
    Utils u = new Utils();

    public Adicionar(Connection con) {
        this.con = con;
    }

    public void addElemento() {      // adicionar elementos a BD na tabela elementos
        try {
            String sql = "Insert into elementos(nome, numero, idEquipa) values (?,?,?)";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Insira o nome:");
            String nome = scan.next();
            System.out.println("Insira um numero:");
            int numero = scan.nextInt();
            System.out.println("Insira um ID de Equipa:");
            String idEquipa = scan.next();
            if (u.checkElementos(nome, numero)) {
                preparedStatement.setString(1, nome);
                preparedStatement.setInt(2, numero);
                preparedStatement.setString(3, idEquipa);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException ex) {
            System.out.print("Error message:" + ex.getMessage());
        }
    }

    public void addEquipa() {      // adicionar equipas a BD na tabela equipas
        try {
            String sql = "Insert into equipa(nomeEquipa, idEquipa, idRobo) values (?,?,?)";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Insira o nome da equipa:");
            String nome = scan.next();
            System.out.println("Insira o id da equipa:");
            String id = scan.next();
            System.out.println("Insira um id de robo:");
            int idRobo = scan.nextInt();
                preparedStatement.setString(1, nome);
                preparedStatement.setString(2, id);
                preparedStatement.setInt(3, idRobo);
                preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.print("Error message:" + ex.getMessage());
        }
    }

    public void addProva() {      // adicionar provas a BD na tabela provas
        try {
            String sql = "Insert into prova(dia, duracao, pontuacao,robo_ID, local,id) values (?,?,?,?,?,?)";
            preparedStatement = this.con.prepareStatement(sql);
            java.util.Date utilDate = new java.util.Date(); // converte date do java para date do mysql
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            System.out.println("Quanto tempo durou:");
            String tempo = scan.next();
            System.out.println("Pontuação:");
            int pont = scan.nextInt();
            System.out.println("ID do robo:");
            int idRobo = scan.nextInt();
            System.out.println("Local:");
            String local = scan.next();
            System.out.println("ID da prova:");
            int id = scan.nextInt();
            preparedStatement.setDate(1, sqlDate);
            preparedStatement.setString(2, tempo);
            preparedStatement.setInt(3, pont);
            preparedStatement.setInt(4, idRobo);
            preparedStatement.setString(5, local);
            preparedStatement.setInt(6, id);
            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            System.out.print("Error message:" + ex.getMessage());
        }
    }

    public void addRobo() {      // adicionar robos a BD na tabela robos
        try {
            String sql = "Insert into robo(sonares,componentes,butõesLigação,id) values (?,?,?,?)";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Quantos sonares tem:");
            int sonares = scan.nextInt();
            System.out.println("Introduza as componentes do carro:");
            String comp = scan.next();
            System.out.println("Botões de Ligação");
            String bl = scan.next();
            System.out.println("ID:");
            int ID = scan.nextInt();
            preparedStatement.setInt(1, sonares);
            preparedStatement.setString(2, comp);
            preparedStatement.setString(3, bl);
            preparedStatement.setInt(4, ID);
            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            System.out.print("Error message:" + ex.getMessage());
        }
    }
}
