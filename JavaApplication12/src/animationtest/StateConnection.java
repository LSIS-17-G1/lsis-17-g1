/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animationtest;

import java.awt.Color;

/**
 *
 * @author Henrique
 */
public class StateConnection {

    State startingState;
    State endingState;
    boolean active = false;
    Color defaultColor = Color.black;
    Color activeColor = Color.red;
    boolean blink = false;

    public void establishConnection(State start, State end) {
        startingState = start;
        endingState = end;
    }

    public void changeActive(boolean change) {
        active = change;
    }

    public Color getColor() {
        if (active && !blink) {
            return activeColor;
        } else {
            return defaultColor;
        }
    }

    public void blinkOnce() {
        blink = !blink;
    }
}
