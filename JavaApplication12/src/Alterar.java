/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.*;
import java.util.Scanner;

/**
 *
 * @author Henrique
 */
public class Alterar {

    public Connection con;
    public PreparedStatement preparedStatement;
    public ResultSet resultSet;
    Scanner scan = new Scanner(System.in);
    Utils u = new Utils();

    public Alterar(Connection con) {
        this.con = con;
    }

    public void Elemento() {
        try {
            String sql = "update elementos set elementos.nome=?, elementos.numero=?, elementos.idEquipa=? where elementos.numero=? ";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Qual é o numero do elemento que deseja alterar?");
            int n = scan.nextInt();
            System.out.println("Qual o nome que deseja inserir:");
            String nome = scan.next();
            System.out.println("Qual o numero que deseja inserir:");
            int nu = scan.nextInt();
            System.out.println("Qual o ID de equipa que deseja inserir:");
            String idEquipa = scan.next();
            if (u.checkElementos(nome, nu)) {
                preparedStatement.setString(1, nome);
                preparedStatement.setInt(2, nu);
                preparedStatement.setString(3, idEquipa);
                preparedStatement.setInt(4, n);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException ex) {
            System.out.print("Error Message: " + ex.getMessage());
        }
    }

    public void Equipa() {
        try {
            String sql = "update equipa set equipa.nomeEquipa=?, equipa.idEquipa=?, equipa.idRobo where equipa.idEquipa=? ";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Qual é o ID da equipa que deseja alterar?");
            String ID = scan.next();
            System.out.println("Qual o nome de equipa que deseja inserir:");
            String nomeEquipa = scan.next();
            System.out.println("Qual o ID de equipa que deseja inserir:");
            String NID = scan.next();
            System.out.println("Qual o ID robo que deseja inserir:");
            int idRobo = scan.nextInt();
            if (u.checkEquipas(nomeEquipa, NID)) {
                preparedStatement.setString(1, nomeEquipa);
                preparedStatement.setString(2, NID);
                preparedStatement.setInt(3, idRobo);
                preparedStatement.setString(4, ID);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException ex) {
            System.out.print("Error Message: " + ex.getMessage());
        }
    }

    public void Prova() {
        try {
            String sql = "update prova set prova.dia)=?, prova.duracao=?, prova.pontuacao=?, prova.robo_ID=?, prova.local=?, prova.id=? where prova.id = ? ";
            preparedStatement = this.con.prepareStatement(sql);
            java.util.Date utilDate = new java.util.Date(); // converte date do java para date do mysql
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            System.out.println("Qual é o ID da equipa que deseja alterar?");
            int ID = scan.nextInt();
            System.out.println("Qual a duração que deseja inserir:");
            String duracao = scan.next();
            System.out.println("Qual a pontuação robo que deseja inserir:");
            int pont = scan.nextInt();
            System.out.println("Qual o ID de robo que deseja inserir:");
            String RID = scan.next();
            System.out.println("Qual o local que deseja inserir:");
            String local = scan.next();
            System.out.println("Qual o id da prova:");
            int PId = scan.nextInt();
            preparedStatement.setDate(1, sqlDate);
            preparedStatement.setString(2, duracao);
            preparedStatement.setInt(3, pont);
            preparedStatement.setString(4, RID);
            preparedStatement.setString(5, local);
            preparedStatement.setInt(6, PId);
            preparedStatement.setInt(7, ID);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.print("Error Message: " + ex.getMessage());
        }
    }

    public void Robo() {
        try {
            String sql = "update robo set robo.sonares=?, robo.componentes=?,robo.butõesLigação=?,robo.id=? where robo.id=? ";
            preparedStatement = this.con.prepareStatement(sql);
            System.out.println("Insira o ID do robo que deseja alterar:");
            int oldId = scan.nextInt();
            System.out.println("Insira o numero de sonares:");
            int son = scan.nextInt();
            System.out.println("Insira as componentes:");
            String comp = scan.next();
            System.out.println("Insira as ligações:");
            String lig = scan.next();
            System.out.println("Insira o ID:");
            int ID = scan.nextInt();
            preparedStatement.setInt(1, son);
            preparedStatement.setString(2, comp);
            preparedStatement.setString(3, lig);
            preparedStatement.setInt(4, ID);
            preparedStatement.setInt(5, oldId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.print("Error Message: " + ex.getMessage());
        }
    }
}
