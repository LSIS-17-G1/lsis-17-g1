/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.*;
import java.util.Date;

/**
 *
 * @author Henrique
 */
public class Pesquisar {

    public Connection con;
    public Statement stmt;
    private ResultSet resultSet;

    public Pesquisar(Connection con) {
        try {
            this.con = con;
            stmt = this.con.createStatement();
        } catch (SQLException ex) {
            System.out.print("Error message:" + ex.getMessage());
        }
    }

    public void elementos() {   // pesquisar e dar print de elementos
        try {
            resultSet = stmt.executeQuery("select * from elementos");
            while (resultSet.next()) {
                String nome = resultSet.getString("nome");
                int numero = resultSet.getInt("numero");
                System.out.println("Nome: " + nome + " Numero: " + numero);

            }
        } catch (SQLException ex) {
            System.err.print("Error Message: " + ex.getMessage());
        }
    }

    public void equipas() {   // pesquisar e dar print de elementos
        try {
            resultSet = stmt.executeQuery("select * from equipa");
            while (resultSet.next()) {
                String nomeEquipa = resultSet.getString("nomeEquipa");
                String idEquipa = resultSet.getString("idEquipa");
                System.out.println("Nome da equipa:" + nomeEquipa + " Id:" + idEquipa);
            }
        } catch (SQLException ex) {
            System.err.print("Error Message: " + ex.getMessage());
        }
    }

    public void prova() {   // pesquisar e dar print de elementos
        try {
            resultSet = stmt.executeQuery("select * from prova");
            while (resultSet.next()) {
                Date data = resultSet.getDate("dia");
                int duracao = resultSet.getInt("duracao");
                int pontuacao = resultSet.getInt("pontuacao");
                String local = resultSet.getString("local");
                int idRobo = resultSet.getInt("robo_ID");
                int idProva = resultSet.getInt("id");
                System.out.println("Dia: " + data + " Duração:" + duracao + " Pontuação:" + pontuacao + " Local:" + local + " IDRobo:" + idRobo + " IDProva:" + idProva);
            }
        } catch (SQLException ex) {
            System.err.print("Error Message: " + ex.getMessage());
        }
    }

    public void robo() {   // pesquisar e dar print de elementos
        try {
            resultSet = stmt.executeQuery("select * from robo");
            while (resultSet.next()) {
                int sonares = resultSet.getInt("sonares");
                String comp = resultSet.getString("componentes");
                String bl = resultSet.getString("butõesLigação");
                int ID = resultSet.getInt("id");
                System.out.println("Sonares:" + sonares + " Componentes:" + comp + " Butões Ligação:" + bl + " ID:" + ID);
            }
        } catch (SQLException ex) {
            System.err.print("Error Message: " + ex.getMessage());
        }
    }

}
