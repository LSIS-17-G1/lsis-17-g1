/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animationtest;

import java.awt.Color;
import java.awt.List;
import java.util.ArrayList;

/**
 *
 * @author Henrique
 */
public class State {

    int x;
    int y;
    ArrayList<State> statesTo;
    boolean active = false;
    String text;
    Color defaultColor = Color.LIGHT_GRAY;
    Color activeColor = Color.green;
    Color currentColor;
    boolean blink = false;

    public State(int _x, int _y, String _text) {
        x = _x;
        y = _y;
        text = _text;
        this.statesTo = new ArrayList();
    }

    public void addStateTo(State state) {
        statesTo.add(state);
    }

    public void changeActive(boolean change) {
        active = change;
    }

    public Color getColor() {
        if (active && !blink) {
            return activeColor;
        } else {
            return defaultColor;
        }
    }

    public void blinkOnce() {
        blink = !blink;
    }

}
