/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Scanner;

/**
 *
 * @author Henrique
 */
public class Menu {

    Scanner scan = new Scanner(System.in);
    Connect con = new Connect();
    Adicionar a = new Adicionar(con.Connect());
    Pesquisar s = new Pesquisar(con.Connect());
    Remover d = new Remover(con.Connect());
    Alterar al = new Alterar(con.Connect());
    boolean m1 = true, m2 = true, m3 = true, m4 = true, m5 = true;

    public Menu() {
        while (m1) {
            System.out.print("Para adicionar algo: Prima 1 \nPara procurar algo: Prima 2 \nPara remover algo: Prima 3\nPara alterar algo: Prima 4 \nPara sair: Prima 0\n");
            int m = scan.nextInt();
            switch (m) {
                case 1:
                    add();
                    break;
                case 2:
                    search();
                    break;
                case 3:
                    delete();
                    break;
                case 4:
                    change();
                    break;
                default:
                    System.out.println("Numero Invalido!");     // Verificação de segurança
                    break;
                case 0:
                    m1 = false;
                    break;
            }
        }
    }

    public void add() {
        while (m2) {
            System.out.println("Para adicionar Elementos: Prima 1 \nPara adicionar Equipas: Prima 2 \nPara adicionar Provas: Prima 3\nPara adicionar Robos: Prima 4\nPara sair: Prima 0");
            int ad = scan.nextInt();
            switch (ad) {
                case 1:
                    a.addElemento();
                    break;
                case 2:
                    a.addEquipa();
                    break;
                case 3:
                    a.addProva();
                    break;
                case 4:
                    a.addRobo();
                    break;
                case 0:
                    m2 = false;
                    break;
            }
        }
    }

    public void search() {
        while (m3) {
            System.out.println("Para pesquisar Elementos: Prima 1 \nPara pesquisar Equipas: Prima 2 \nPara pesquisar Provas: Prima 3\nPara pesquisar Robos: Prima 4\nPara sair: Prima 0");
            int sc = scan.nextInt();
            switch (sc) {
                case 1:
                    s.elementos();
                    break;
                case 2:
                    s.equipas();
                    break;
                case 3:
                    s.prova();
                    break;
                case 4:
                    s.robo();
                    break;
                case 0:
                    m3 = false;
                    break;
            }
        }
    }

    public void delete() {
        while (m4) {
            System.out.println("Para remover Elementos: Prima 1 \nPara remover Equipas: Prima 2 \nPara remover Provas: Prima 3\nPara remover Robos: Prima 4\nPara sair: Prima 0");
            int de = scan.nextInt();
            switch (de) {
                case 1:
                    d.Elementos();
                    break;
                case 2:
                    d.Equipas();
                    break;
                case 3:
                // d.Prova();
                case 4:
                    d.Robo();
                    break;
                case 0:
                    m4 = false;
                    break;
            }
        }
    }

    public void change() {
        while (m5) {
            System.out.println("Para alterar Elementos: Prima 1 \nPara alterar Equipas: Prima 2 \nPara alterar Provas: Prima 3\nPara alterar Robos: Prima 4\nPara sair: Prima 0");
            int c = scan.nextInt();
            switch (c) {
                case 1:
                    al.Elemento();
                    break;
                case 2:
                    al.Equipa();
                    break;
                case 3:
                // d.Prova();
                case 4:
                    al.Robo();
                    break;
                case 0:
                    m5 = false;
                    break;
            }
        }
    }
}
