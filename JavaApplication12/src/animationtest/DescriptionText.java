/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animationtest;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Henrique
 */
public class DescriptionText {
    
    String text;
    int x;
    int y;
    public List<State> statesAssociated = new ArrayList();
    public List<StateConnection> connectionsAssociated = new ArrayList();
    boolean active = false;
    Color defaultColor = Color.black;
    float[] colorHSB = Color.RGBtoHSB(29, 154, 27, null);
    Color activeColor = Color.getHSBColor(colorHSB[0], colorHSB[1], colorHSB[2]);
    
    public DescriptionText(int _x, int _y, String _text){
        x = _x;
        y = _y;
        text = _text;
    }

    public Color getColor(){
        if(active)
            return activeColor;
        else
            return defaultColor;
    }
    
    public void addState(State state){
        statesAssociated.add(state);
    }
    
    public void addConnection(StateConnection sc){
        connectionsAssociated.add(sc);
    }
}
