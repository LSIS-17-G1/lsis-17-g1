/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animationtest;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class StateMachineAnim extends JPanel implements ActionListener {

    JPanel jp;

    JLabel jS1;
    JLabel jS2;
    JLabel jS3;
    JLabel jS4;
    JLabel jS5;
    JLabel jS6;
    JLabel jS7;

    JLabel j1;
    JLabel j2;
    JLabel j3;
    JLabel j4;
    JLabel j5;
    JLabel j6;
    JLabel j7;

    long last_time = System.nanoTime();
    long timer = 0;
    int circleSize = 50;

    @Override
    public void actionPerformed(ActionEvent e) {

        for (int j = 0; j < connections.size(); j++) {
            if (connections.get(j).active) {
                connections.get(j).blinkOnce();
            }
        }

        for (int k = 0; k < states.size(); k++) {
            if (states.get(k).active) {
                states.get(k).blinkOnce();
            }
        }

        repaint();
    }

    Timer t = new Timer(300, this);

    Font textFont = new Font("Arial Black", Font.BOLD, 20);

    State s1 = new State(75, 150, "S1");
    State s2 = new State(150, 150, "S2");
    State s3 = new State(225, 150, "S3");
    State s4 = new State(225, 225, "S4");
    State s5 = new State(300, 75, "S5");
    State s6 = new State(375, 75, "S6");
    State s7 = new State(450, 75, "S7");

    List<State> states = new ArrayList();

    StateConnection s1_s2 = new StateConnection();

    StateConnection s2_s3 = new StateConnection();
    StateConnection s2_s4 = new StateConnection();
    StateConnection s2_s5 = new StateConnection();

    StateConnection s3_s2 = new StateConnection();
    StateConnection s3_s5 = new StateConnection();

    StateConnection s4_s2 = new StateConnection();
    StateConnection s4_s5 = new StateConnection();

    StateConnection s5_s6 = new StateConnection();

    StateConnection s6_s7 = new StateConnection();

    List<StateConnection> connections = new ArrayList();

    DescriptionText dt_s1 = new DescriptionText(25, 25, "S1 - Idle");
    DescriptionText dt_s2 = new DescriptionText(25, 50, "S2 - Andar Frente");
    DescriptionText dt_s3 = new DescriptionText(25, 75, "S3 - Virar Direita");
    DescriptionText dt_s4 = new DescriptionText(25, 100, "S4 - Virar Esquerda");
    DescriptionText dt_s5 = new DescriptionText(25, 125, "S5 - Aproximar à Chama");
    DescriptionText dt_s6 = new DescriptionText(25, 150, "S6 - Apagar Chama");
    DescriptionText dt_s7 = new DescriptionText(25, 175, "S7 - Chama Apagada");

    DescriptionText dt_1 = new DescriptionText(200, 25, "1 - Botão Verde");
    DescriptionText dt_2 = new DescriptionText(200, 50, "2 - DistDir > x");
    DescriptionText dt_3 = new DescriptionText(200, 75, "3 - DistEsq > x");
    DescriptionText dt_4 = new DescriptionText(200, 100, "4 - DistDir ~= DistEsq");
    DescriptionText dt_5 = new DescriptionText(200, 125, "5 - Deteta Chama");
    DescriptionText dt_6 = new DescriptionText(200, 150, "6 - DistChama OK");
    DescriptionText dt_7 = new DescriptionText(200, 175, "7 - Chama apagada");

    List<DescriptionText> descriptions = new ArrayList();

    public StateMachineAnim() {

        s1_s2.establishConnection(s1, s2);

        s2_s3.establishConnection(s2, s3);
        s2_s4.establishConnection(s2, s4);
        s2_s5.establishConnection(s2, s5);

        s3_s2.establishConnection(s3, s2);
        s3_s5.establishConnection(s3, s5);

        s4_s2.establishConnection(s4, s2);
        s4_s5.establishConnection(s4, s5);

        s5_s6.establishConnection(s5, s6);

        s6_s7.establishConnection(s6, s7);

        states.add(s1);
        states.add(s2);
        states.add(s3);
        states.add(s4);
        states.add(s5);
        states.add(s6);
        states.add(s7);

        connections.add(s1_s2);

        connections.add(s2_s3);
        connections.add(s2_s4);
        connections.add(s2_s5);

        connections.add(s3_s2);
        connections.add(s3_s5);

        connections.add(s4_s2);
        connections.add(s4_s5);

        connections.add(s5_s6);

        connections.add(s6_s7);

        dt_s1.addState(s1);
        dt_s2.addState(s2);
        dt_s3.addState(s3);
        dt_s4.addState(s4);
        dt_s5.addState(s5);
        dt_s6.addState(s6);
        dt_s7.addState(s7);

        dt_1.addConnection(s1_s2);
        dt_2.addConnection(s2_s3);
        dt_3.addConnection(s2_s4);
        dt_4.addConnection(s3_s2);
        dt_4.addConnection(s4_s2);
        dt_5.addConnection(s2_s5);
        dt_5.addConnection(s3_s5);
        dt_5.addConnection(s4_s5);
        dt_6.addConnection(s5_s6);
        dt_7.addConnection(s6_s7);

        descriptions.add(dt_1);
        descriptions.add(dt_2);
        descriptions.add(dt_3);
        descriptions.add(dt_4);
        descriptions.add(dt_5);
        descriptions.add(dt_6);
        descriptions.add(dt_7);

        descriptions.add(dt_s1);
        descriptions.add(dt_s2);
        descriptions.add(dt_s3);
        descriptions.add(dt_s4);
        descriptions.add(dt_s5);
        descriptions.add(dt_s6);
        descriptions.add(dt_s7);

        jp = new JPanel();
        this.add(jp);
        jp.setOpaque(true);

        jS1 = new JLabel();
        jp.add(jS1);
        jS1.setVisible(true);
        jS2 = new JLabel();
        jp.add(jS2);
        jS2.setVisible(true);
        jS3 = new JLabel();
        jp.add(jS3);
        jS3.setVisible(true);
        jS4 = new JLabel();
        jp.add(jS4);
        jS4.setVisible(true);
        jS5 = new JLabel();
        jp.add(jS5);
        jS5.setVisible(true);
        jS6 = new JLabel();
        jp.add(jS6);
        jS6.setVisible(true);
        jS7 = new JLabel();
        jp.add(jS7);
        jS7.setVisible(true);

        j1 = new JLabel();
        jp.add(j1);
        j1.setVisible(true);
        j2 = new JLabel();
        jp.add(j2);
        j2.setVisible(true);
        j3 = new JLabel();
        jp.add(j3);
        j3.setVisible(true);
        j4 = new JLabel();
        jp.add(j4);
        j4.setVisible(true);
        j5 = new JLabel();
        jp.add(j5);
        j5.setVisible(true);
        j6 = new JLabel();
        jp.add(j6);
        j6.setVisible(true);
        j7 = new JLabel();
        jp.add(j7);
        j7.setVisible(true);

        jp.setBounds(600, 0, 400, 400);

        repaint();

    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (int i = 0; i < connections.size(); i++) {
            drawConnection(g, connections.get(i).startingState.x, connections.get(i).startingState.y,
                    connections.get(i).endingState.x, connections.get(i).endingState.y,
                    connections.get(i).getColor(), connections.get(i).active);
        }

        for (int i = 0; i < states.size(); i++) {
            drawState(g, states.get(i).x, states.get(i).y, circleSize, states.get(i).getColor(), states.get(i).text);
        }

        jS1.setForeground(dt_s1.getColor());
        jS1.setText(dt_s1.text);
        jS1.setLocation(dt_s1.x, dt_s1.y);
        jS2.setForeground(dt_s2.getColor());
        jS2.setText(dt_s2.text);
        jS2.setLocation(dt_s2.x, dt_s2.y);
        jS3.setForeground(dt_s3.getColor());
        jS3.setText(dt_s3.text);
        jS3.setLocation(dt_s3.x, dt_s3.y);
        jS4.setForeground(dt_s4.getColor());
        jS4.setText(dt_s4.text);
        jS4.setLocation(dt_s4.x, dt_s4.y);
        jS5.setForeground(dt_s5.getColor());
        jS5.setText(dt_s5.text);
        jS5.setLocation(dt_s5.x, dt_s5.y);
        jS6.setForeground(dt_s6.getColor());
        jS6.setText(dt_s6.text);
        jS6.setLocation(dt_s6.x, dt_s6.y);
        jS7.setForeground(dt_s7.getColor());
        jS7.setText(dt_s7.text);
        jS7.setLocation(dt_s7.x, dt_s7.y);

        j1.setForeground(dt_1.getColor());
        j1.setText(dt_1.text);
        j1.setLocation(dt_1.x, dt_1.y);
        j2.setForeground(dt_2.getColor());
        j2.setText(dt_2.text);
        j2.setLocation(dt_2.x, dt_2.y);
        j3.setForeground(dt_3.getColor());
        j3.setText(dt_3.text);
        j3.setLocation(dt_3.x, dt_3.y);
        j4.setForeground(dt_4.getColor());
        j4.setText(dt_4.text);
        j4.setLocation(dt_4.x, dt_4.y);
        j5.setForeground(dt_5.getColor());
        j5.setText(dt_5.text);
        j5.setLocation(dt_5.x, dt_5.y);
        j6.setForeground(dt_6.getColor());
        j6.setText(dt_6.text);
        j6.setLocation(dt_6.x, dt_6.y);
        j7.setForeground(dt_7.getColor());
        j7.setText(dt_7.text);
        j7.setLocation(dt_7.x, dt_7.y);

        jp.setBounds(600, 0, 400, 400);

        long time = System.nanoTime();
        int delta_time = (int) ((time - last_time) / 1000000);
        last_time = time;
        timer += delta_time;

        if (timer >= 0 && timer < 3000) {
            state1();
        }

        if (timer >= 3000 && timer < 5000) {
            connectionS1_S2();
        }

        if (timer > 5000 && timer < 7000) {
            state2();
        }

        if (timer > 7000 && timer < 9000) {
            connectionS2_S3();
        }

        if (timer > 9000 && timer < 11000) {
            state3();
        }

        if (timer > 11000 && timer < 13000) {
            connectionS3_S2();
        }

        if (timer > 13000 && timer < 15000) {
            state2();
        }

        if (timer > 15000 && timer < 17000) {
            connectionS2_S4();
        }

        if (timer > 17000 && timer < 19000) {
            state4();
        }

        if (timer > 19000 && timer < 21000) {
            connectionS4_S5();
        }

        if (timer > 21000 && timer < 23000) {
            state5();
        }

        if (timer > 23000 && timer < 25000) {
            connectionS5_S6();
        }

        if (timer > 25000 && timer < 27000) {
            state6();
        }

        if (timer > 27000 && timer < 29000) {
            connectionS6_S7();
        }

        if (timer > 29000 && timer < 32000) {
            state7();
        }

        if (timer > 31000) {
            ResetAnimation();
        }

        CheckTextColor();

        t.start();
    }

    void ResetAnimationButNotTimer() {
        for (int i = 0; i < states.size(); i++) {
            states.get(i).active = false;
        }

        for (int i = 0; i < connections.size(); i++) {
            connections.get(i).active = false;
        }

        for (int i = 0; i < descriptions.size(); i++) {
            descriptions.get(i).active = false;
        }
    }

    void CheckTextColor() {
        for (int i = 0; i < descriptions.size(); i++) {
            for (int k = 0; k < descriptions.get(i).statesAssociated.size(); k++) {
                for (int j = 0; j < states.size(); j++) {
                    if (descriptions.get(i).statesAssociated.get(k) == states.get(j) && states.get(j).active) {
                        descriptions.get(i).active = true;
                        break;
                    }
                }
            }

            for (int k = 0; k < descriptions.get(i).connectionsAssociated.size(); k++) {
                for (int j = 0; j < connections.size(); j++) {
                    if (descriptions.get(i).connectionsAssociated.get(k) == connections.get(j) && connections.get(j).active) {
                        descriptions.get(i).active = true;
                        break;
                    }
                }
            }
        }
    }

    void ResetAnimation() {
        for (int i = 0; i < states.size(); i++) {
            states.get(i).active = false;
        }

        for (int i = 0; i < connections.size(); i++) {
            connections.get(i).active = false;
        }

        for (int i = 0; i < descriptions.size(); i++) {
            descriptions.get(i).active = false;
        }

        timer = 0;
    }

    void state1() {
        ResetAnimationButNotTimer();
        s1.active = true;
    }

    void state2() {
        ResetAnimationButNotTimer();
        s2.active = true;
    }

    void state3() {
        ResetAnimationButNotTimer();
        s3.active = true;
    }

    void state4() {
        ResetAnimationButNotTimer();
        s4.active = true;
    }

    void state5() {
        ResetAnimationButNotTimer();
        s5.active = true;
    }

    void state6() {
        ResetAnimationButNotTimer();
        s6.active = true;
    }

    void state7() {
        ResetAnimationButNotTimer();
        s7.active = true;
    }

    void connectionS1_S2() {
        ResetAnimationButNotTimer();
        s1_s2.active = true;
    }

    void connectionS2_S3() {
        ResetAnimationButNotTimer();
        s2_s3.active = true;
    }

    void connectionS2_S4() {
        ResetAnimationButNotTimer();
        s2_s4.active = true;
    }

    void connectionS2_S5() {
        ResetAnimationButNotTimer();
        s2_s5.active = true;
    }

    void connectionS3_S2() {
        ResetAnimationButNotTimer();
        s3_s2.active = true;
    }

    void connectionS3_S5() {
        ResetAnimationButNotTimer();
        s3_s5.active = true;
    }

    void connectionS4_S2() {
        ResetAnimationButNotTimer();
        s4_s2.active = true;
    }

    void connectionS4_S5() {
        ResetAnimationButNotTimer();
        s4_s5.active = true;
    }

    void connectionS5_S6() {
        ResetAnimationButNotTimer();
        s5_s6.active = true;
    }

    void connectionS6_S7() {
        ResetAnimationButNotTimer();
        s6_s7.active = true;
    }

    public void drawState(Graphics g, int x, int y, int size, Color color, String text) {
        g.setColor(color);
        g.fillArc(x, y, size, size, 0, 360);
        g.setColor(Color.BLACK);
        g.setFont(textFont);
        g.drawString(text, x + 10, y + 35);
    }

    public void drawConnection(Graphics g, int xi, int yi, int xf, int yf, Color color, boolean active) {
        g.setColor(color);
        g.drawLine(xi + 25, yi + 25, xf + 25, yf + 25);
        drawConnectionArrow(xi + 25, yi + 25, xf + 25, yf + 25, g);
    }

    void drawConnectionArrow(int x1, int y1, int x2, int y2, Graphics g) {

        int x[] = new int[3];
        int y[] = {-10, 0, 10};

        if (x1 < x2) {
            x[0] = 5;
            x[1] = 15;
            x[2] = 5;
        } else {
            x[0] = -5;
            x[1] = -15;
            x[2] = -5;
        }

        Graphics2D g2d = (Graphics2D) g;

        int xDiff = Math.abs(x2 - x1) / 2;
        int yDiff = Math.abs(y2 - y1) / 2;

        if (x1 < x2) {
            xDiff += x1;
        } else {
            xDiff += x2;
        }

        if (y1 < y2) {
            yDiff += y1;
        } else {
            yDiff += y2;
        }

        g2d.translate(xDiff, yDiff);
        double angle = 0;
        angle = findConnectionAngle(x1, y1, x2, y2);
        g2d.rotate(angle);

        g2d.fillPolygon(new Polygon(x, y, 3));

        g2d.rotate(-angle);
        g2d.translate(-xDiff, -yDiff);
    }

    private double findConnectionAngle(int x1, int y1, int x2, int y2) {
        if ((x2 - x1) == 0) {
            if (y1 < y2) {
                return Math.toRadians(270);
            } else {
                return Math.toRadians(90);
            }
        }

        int deltaY = y2 - y1;
        int deltaX = x2 - x1;
        double math = deltaY / deltaX;
        return Math.atan(math);
    }
}
